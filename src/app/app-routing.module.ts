import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomExerciseComponent } from './custom-exercise/custom-exercise.component';
import { ExerciseComponent } from './exercise/exercise.component';
import { ExercisesComponent } from './exercises/exercises.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/exercises', pathMatch: 'full' },
  { path: 'custom-exercise', component: CustomExerciseComponent },
  { path: 'exercise/:id', component: ExerciseComponent },
  { path: 'exercises', component: ExercisesComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
