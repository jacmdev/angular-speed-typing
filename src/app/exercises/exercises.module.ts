import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ExercisesComponent } from './exercises.component';

@NgModule({
  declarations: [ExercisesComponent],
  imports: [CommonModule, RouterModule],
})
export class ExercisesModule {}
