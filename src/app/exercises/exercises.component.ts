import { Component, OnInit } from '@angular/core';
import { Exercise } from '@app/shared/interfaces/exercise';
import { ExercisesService } from './exercises.service';

@Component({
  templateUrl: './exercises.component.html',
  styleUrls: ['./exercises.component.scss'],
  providers: [ExercisesService],
})
export class ExercisesComponent implements OnInit {
  exercises: Exercise[] = [];

  constructor(private service: ExercisesService) {}

  ngOnInit(): void {
    this.service.getExercises().subscribe((response: Exercise[]) => (this.exercises = response));
  }
}
