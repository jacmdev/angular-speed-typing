import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable()
export class UtilsService {
  static markControlsAsTouched(form: FormGroup): void {
    Object.keys(form.controls).forEach((control) => {
      form.controls[control].markAsTouched();
    });
  }
}
