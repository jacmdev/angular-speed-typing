export interface Options {
  message: string;
  type?: 'danger' | 'info';
}
