import { Injectable } from '@angular/core';
import { environment } from '@environment';
import { BehaviorSubject } from 'rxjs';
import { Data } from './data';
import { Options } from './options';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  data$ = new BehaviorSubject<Data>({ isRendered: false, isVisible: false, message: '', type: '' });

  hide(): void {
    const data = { ...this.data$.getValue(), isRendered: true, isVisible: false };
    this.data$.next(data);
    window.setTimeout(() => this.data$.next({ ...data, isRendered: false }), environment.transitionDuration);
  }

  show(options: Options): void {
    const data = { isRendered: true, isVisible: false, message: options.message, type: options.type || 'info' };
    this.data$.next(data);
    window.setTimeout(() => this.data$.next({ ...data, isVisible: true }), 50);
  }
}
