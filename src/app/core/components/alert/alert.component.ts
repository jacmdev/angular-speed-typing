import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from './alert.service';
import { Data } from './data';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit {
  isRendered = false;
  isVisible = false;
  message = '';
  type = '';

  private subscription = new Subscription();

  constructor(private service: AlertService) {}

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription.add(
      this.service.data$.subscribe((data: Data) => {
        this.isRendered = data.isRendered;
        this.isVisible = data.isVisible;
        this.message = data.message;
        this.type = data.type;
      })
    );
  }

  onClose(): void {
    this.service.hide();
  }
}
