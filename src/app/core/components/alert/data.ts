export interface Data {
  isRendered: boolean;
  isVisible: boolean;
  message: string;
  type: string;
}
