import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  isRendered$ = new BehaviorSubject<boolean>(false);

  hide(): void {
    this.isRendered$.next(false);
  }

  show(): void {
    this.isRendered$.next(true);
  }
}
