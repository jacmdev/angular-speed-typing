import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-mobile-menu-btn',
  templateUrl: './mobile-menu-btn.component.html',
  styleUrls: ['./mobile-menu-btn.component.scss'],
})
export class MobileMenuBtnComponent {
  @Input() isMenuVisible = false;
  @Output() toggle = new EventEmitter<void>();

  onToggle(): void {
    this.toggle.emit();
  }
}
