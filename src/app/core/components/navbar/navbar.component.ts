import { Component } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  isMobileMenuVisible = false;
  menuItems = [
    { label: 'Exercises', link: '/exercises' },
    { label: 'Custom exercise', link: '/custom-exercise' },
  ];

  onMobileMenuClose(): void {
    this.isMobileMenuVisible = false;
  }

  onMobileMenuToggle(): void {
    this.isMobileMenuVisible = !this.isMobileMenuVisible;
  }
}
