import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@environment';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let url = request.url;

    if (url.startsWith('/api')) {
      url = url.replace(/^\/api(?=\/)/, environment.apiUrl);
      return next.handle(request.clone({ url }));
    }

    return next.handle(request);
  }
}
