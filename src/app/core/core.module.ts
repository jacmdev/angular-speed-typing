import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { AlertComponent } from './components/alert/alert.component';
import { LoaderComponent } from './components/loader/loader.component';
import { MobileMenuComponent } from './components/navbar/mobile-menu/mobile-menu.component';
import { MobileMenuBtnComponent } from './components/navbar/mobile-menu-btn/mobile-menu-btn.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ApiInterceptor } from './interceptors/api.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { LoaderInterceptor } from './interceptors/loader.interceptor';

@NgModule({
  declarations: [AlertComponent, LoaderComponent, MobileMenuComponent, MobileMenuBtnComponent, NavbarComponent],
  imports: [CommonModule, RouterModule, SharedModule],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  exports: [AlertComponent, LoaderComponent, NavbarComponent],
})
export class CoreModule {}
