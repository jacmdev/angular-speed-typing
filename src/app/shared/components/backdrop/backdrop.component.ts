import { Component, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-backdrop',
  template: '<div class="backdrop" [style.z-index]="isUnderNavbar ? 18 : null" (click)="onClick()"></div>',
  styleUrls: ['./backdrop.component.scss'],
})
export class BackdropComponent {
  @Input() isUnderNavbar = false;
  click = new EventEmitter<void>();

  onClick(): void {
    this.click.emit();
  }
}
