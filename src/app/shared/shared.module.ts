import { NgModule } from '@angular/core';
import { BackdropComponent } from './components/backdrop/backdrop.component';

@NgModule({
  declarations: [BackdropComponent],
  exports: [BackdropComponent],
})
export class SharedModule {}
