export interface Exercise {
  id: number;
  text: string;
  title: string;
}
