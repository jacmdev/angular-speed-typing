import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { ModalComponent } from './modal.component';
import { ModalService } from './modal.service';

@NgModule({
  declarations: [ModalComponent],
  imports: [CommonModule, SharedModule],
  providers: [ModalService],
  exports: [ModalComponent],
})
export class ModalModule {}
