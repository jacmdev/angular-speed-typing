import { Component, HostListener, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Data } from './data';
import { ModalService } from './modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  isRendered = false;
  isVisible = false;
  private subscription = new Subscription();

  constructor(public service: ModalService) {}

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription.add(
      this.service.data$.subscribe((data: Data) => {
        this.isRendered = data.isRendered;
        this.isVisible = data.isVisible;
      })
    );
  }

  onClose(): void {
    this.service.hide();
  }

  @HostListener('document:keydown', ['$event'])
  private handleEscapeKeyboardEvent(event: any): void {
    if (this.isRendered && event.key == 'Escape') {
      event.preventDefault();
      this.service.hide();
    }
  }
}
