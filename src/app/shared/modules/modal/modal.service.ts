import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from '@environment';
import { Data } from './data';

@Injectable()
export class ModalService {
  data$ = new BehaviorSubject<Data>({ isRendered: false, isVisible: false });

  hide(): void {
    this.data$.next({ isRendered: true, isVisible: false });
    window.setTimeout(() => this.data$.next({ isRendered: false, isVisible: false }), environment.transitionDuration);
  }

  show(): void {
    this.data$.next({ isRendered: true, isVisible: false });
    window.setTimeout(() => this.data$.next({ isRendered: true, isVisible: true }), 50);
  }
}
