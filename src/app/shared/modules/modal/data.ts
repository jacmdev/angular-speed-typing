export interface Data {
  isRendered: boolean;
  isVisible: boolean;
}
