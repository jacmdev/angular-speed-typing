import { Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { environment } from '@environment';
import { ModalService } from '../modal/modal.service';
import { Char } from './char';
import { ExerciseCoreService } from './exercise-core.service';

@Component({
  selector: 'app-exercise-core',
  templateUrl: './exercise-core.component.html',
  styleUrls: ['./exercise-core.component.scss'],
  providers: [ExerciseCoreService],
})
export class ExerciseCoreComponent implements OnDestroy, OnInit {
  @Input() text = '';
  @Output() typingStart = new EventEmitter<void>();
  chars: Char[] = [];
  penaltyInSeconds = 0;
  score = 0;
  timeWithPenalty = 0;
  private isTypingStarted = false;
  private subscription = new Subscription();

  constructor(private modal: ModalService, private service: ExerciseCoreService) {}

  ngOnDestroy(): void {
    this.service.reset();
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.service.createChars(this.text);
    this.chars = this.service.chars;
    this.handleUpdateData();
  }

  onRestartFromModal(): void {
    this.modal.hide();
    window.setTimeout(() => this.service.reset(), environment.transitionDuration);
  }

  @HostListener('document:keydown', ['$event'])
  private handleBackspaceKeyboardEvent(event: KeyboardEvent): void {
    if (event.key == 'Backspace') {
      event.preventDefault();
      this.service.handleBackspaceKey();
    }
  }

  @HostListener('document:keypress', ['$event'])
  private handleStandardKeyboardEvent(event: KeyboardEvent): void {
    event.preventDefault();
    this.service.handleStandardKey(event.key);
    if (!this.isTypingStarted) {
      this.isTypingStarted = true;
      this.typingStart.emit();
    }
  }

  private handleUpdateData(): void {
    this.subscription.add(
      this.service.updateData$.subscribe((data) => {
        this.penaltyInSeconds = data.penaltyInSeconds;
        this.score = data.score;
        this.timeWithPenalty = data.timeWithPenalty;
      })
    );
  }
}
