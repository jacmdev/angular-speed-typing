import { Component, Input } from '@angular/core';
import { ExerciseCoreService } from '../exercise-core.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent {
  @Input() penaltyInSeconds = 0;
  @Input() score = 0;
  @Input() timeWithPenalty = 0;
  secondPlural: { [key: string]: string } = { '=1': 'second', other: 'seconds' };

  constructor(private service: ExerciseCoreService) {}

  onRestart(event: MouseEvent): void {
    (event.target as HTMLButtonElement).blur();
    this.service.reset();
  }
}
