export interface Char {
  isCorrect?: boolean;
  isCurrent?: boolean;
  isIncorrect?: boolean;
  value: string;
}
