export interface UpdateData {
  penaltyInSeconds: number;
  score: number;
  timeWithPenalty: number;
}
