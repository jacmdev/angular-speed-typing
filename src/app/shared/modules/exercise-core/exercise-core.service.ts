import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalService } from '../modal/modal.service';
import { Char } from './char';
import { UpdateData } from './update-data';

@Injectable()
export class ExerciseCoreService {
  chars: Char[] = [];
  updateData$ = new Subject<UpdateData>();
  private readonly INCORRECT_CHAR_PENALTY = 1000;
  private readonly UPDATE_DATA_INTERVAL = 500;
  private correctCharsCount = 0;
  private currentCharIndex = 0;
  private incorrectCharsCount = 0;
  private isFinished = false;
  private isStarted = false;
  private startTime!: number;
  private updateDataTimer!: number;

  constructor(private modal: ModalService) {}

  createChars(text: string): void {
    this.chars = text.split('').map((char) => ({ value: char }));
    this.chars[0].isCurrent = true;
  }

  handleBackspaceKey(): void {
    if (this.currentCharIndex === 0 || this.isFinished) {
      return;
    }

    const currentChar = this.chars[this.currentCharIndex];
    const previousChar = this.chars[this.currentCharIndex - 1];

    if (previousChar.isCorrect) {
      this.correctCharsCount--;
    } else if (previousChar.isIncorrect) {
      this.incorrectCharsCount--;
    }

    delete previousChar.isCorrect;
    delete previousChar.isIncorrect;
    delete currentChar.isCurrent;
    previousChar.isCurrent = true;

    this.currentCharIndex--;
  }

  handleStandardKey(userChar: string): void {
    if (this.isFinished) {
      return;
    }

    if (!this.isStarted) {
      this.start();
    }

    const currentChar = this.chars[this.currentCharIndex];
    const nextChar = this.chars[this.currentCharIndex + 1];
    const isCorrect = userChar === currentChar.value;
    const isLastChar = this.currentCharIndex === this.chars.length - 1;

    if (isCorrect) {
      currentChar.isCorrect = true;
      this.correctCharsCount++;
    } else {
      currentChar.isIncorrect = true;
      this.incorrectCharsCount++;
    }

    currentChar.isCurrent = false;

    if (isLastChar) {
      this.finish();
    } else {
      nextChar.isCurrent = true;
      this.currentCharIndex++;
    }
  }

  reset(): void {
    window.clearInterval(this.updateDataTimer);
    this.correctCharsCount = 0;
    this.currentCharIndex = 0;
    this.incorrectCharsCount = 0;
    this.isFinished = false;
    this.isStarted = false;
    this.startTime = Date.now();
    this.chars.forEach((char) => {
      delete char.isCorrect;
      delete char.isCurrent;
      delete char.isIncorrect;
    });
    this.chars[0].isCurrent = true;
    this.updateData();
  }

  private finish(): void {
    this.isFinished = true;
    this.updateData();
    this.modal.show();
    window.clearInterval(this.updateDataTimer);
  }

  private start(): void {
    this.isStarted = true;
    this.startTime = Date.now();
    this.updateDataTimer = window.setInterval(() => this.updateData(), this.UPDATE_DATA_INTERVAL);
  }

  private updateData(): void {
    const time = Date.now() - this.startTime;
    const penalty = this.incorrectCharsCount * this.INCORRECT_CHAR_PENALTY;
    const penaltyInSeconds = Math.round(penalty / 1000);
    const timeWithPenalty = time + penalty;
    const score = Math.round((this.correctCharsCount * 60) / (timeWithPenalty / 1000)) || 0;
    this.updateData$.next({ penaltyInSeconds, score, timeWithPenalty });
  }
}
