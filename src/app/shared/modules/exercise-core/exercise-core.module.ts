import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalModule } from '../modal/modal.module';
import { ExerciseCoreComponent } from './exercise-core.component';
import { InfoComponent } from './info/info.component';

@NgModule({
  declarations: [ExerciseCoreComponent, InfoComponent],
  imports: [CommonModule, ModalModule],
  exports: [ExerciseCoreComponent],
})
export class ExerciseCoreModule {}
