import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ExerciseCoreModule } from '@app/shared/modules/exercise-core/exercise-core.module';
import { ExerciseComponent } from './exercise.component';

@NgModule({
  declarations: [ExerciseComponent],
  imports: [CommonModule, ExerciseCoreModule],
})
export class ExerciseModule {}
