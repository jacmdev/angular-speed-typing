import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertService } from '@app/core/components/alert/alert.service';
import { Exercise } from '@app/shared/interfaces/exercise';
import { ExerciseService } from './exercise.service';

@Component({
  template: '<app-exercise-core *ngIf="text" [text]="text" (typingStart)="onTypingStart()"></app-exercise-core>',
  providers: [ExerciseService],
})
export class ExerciseComponent implements OnDestroy, OnInit {
  text = '';
  private subscription = new Subscription();

  constructor(private alert: AlertService, private route: ActivatedRoute, private service: ExerciseService) {}

  ngOnDestroy(): void {
    this.alert.hide();
  }

  ngOnInit(): void {
    this.subscription.add(
      this.route.params.subscribe((params) => {
        this.service.getExercise(params.id).subscribe((response: Exercise) => {
          this.text = response.text;
          this.alert.show({ message: 'Start typing to begin exercise' });
        });
      })
    );
  }

  onTypingStart(): void {
    this.alert.hide();
  }
}
