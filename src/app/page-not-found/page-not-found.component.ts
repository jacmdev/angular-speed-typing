import { Component } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `
    <div class="not-found">
      <i class="fa fa-exclamation-triangle not-found-icon"></i>
      <div>Page not found</div>
    </div>
  `,
  styleUrls: ['./page-not-found.component.scss'],
})
export class PageNotFoundComponent {}
