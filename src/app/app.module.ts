import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { CustomExerciseModule } from './custom-exercise/custom-exercise.module';
import { ExerciseModule } from './exercise/exercise.module';
import { ExercisesModule } from './exercises/exercises.module';
import { PageNotFoundModule } from './page-not-found/page-not-found.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CoreModule,
    CustomExerciseModule,
    ExerciseModule,
    ExercisesModule,
    PageNotFoundModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
