import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ExerciseCoreModule } from '@app/shared/modules/exercise-core/exercise-core.module';
import { CustomExerciseComponent } from './custom-exercise.component';
import { FormComponent } from './form/form.component';

@NgModule({
  declarations: [CustomExerciseComponent, FormComponent],
  imports: [CommonModule, ReactiveFormsModule, ExerciseCoreModule],
})
export class CustomExerciseModule {}
