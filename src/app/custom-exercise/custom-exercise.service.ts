import { Injectable } from '@angular/core';
import { FormValue } from './form-value';

@Injectable()
export class CustomExerciseService {
  private readonly STORAGE_KEY = 'customExercise';

  loadFromStorage(): FormValue | null {
    const value = localStorage.getItem(this.STORAGE_KEY);
    return value ? JSON.parse(value) : null;
  }

  saveToStorage(value: FormValue): void {
    localStorage.setItem(this.STORAGE_KEY, JSON.stringify(value));
  }
}
