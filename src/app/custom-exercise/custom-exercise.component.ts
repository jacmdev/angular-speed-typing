import { Component, OnInit } from '@angular/core';
import { CustomExerciseService } from './custom-exercise.service';
import { FormValue } from './form-value';

@Component({
  templateUrl: './custom-exercise.component.html',
  providers: [CustomExerciseService],
})
export class CustomExerciseComponent implements OnInit {
  isEditMode = true;
  text = '';

  constructor(private service: CustomExerciseService) {}

  ngOnInit(): void {
    this.loadFromStorage();
  }

  onEdit(): void {
    this.isEditMode = true;
  }

  onFormChange(value: FormValue): void {
    this.text = value.text;
    this.isEditMode = false;
  }

  private loadFromStorage(): void {
    const formValue = this.service.loadFromStorage();
    if (formValue && formValue.text) {
      this.text = formValue.text;
      this.isEditMode = false;
    }
  }
}
