import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UtilsService } from '@app/core/services/utils.service';
import { CustomExerciseService } from '../custom-exercise.service';
import { FormValue } from '../form-value';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent implements OnDestroy, OnInit {
  @Input() defaultText = '';
  @Output() formChange = new EventEmitter<FormValue>();
  form: FormGroup;
  private subscription = new Subscription();

  constructor(private service: CustomExerciseService) {
    this.form = new FormGroup({
      text: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(500)]),
    });
  }

  get textControl(): FormControl {
    return this.form.get('text') as FormControl;
  }

  hasError(controlName: string): boolean {
    const control = this.form.controls[controlName];
    return !control.valid && control.touched;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.form.get('text')?.patchValue(this.defaultText);
    this.handleTextInput();
  }

  onSubmit(): void {
    if (!this.form.valid) {
      UtilsService.markControlsAsTouched(this.form);
      return;
    }

    const value = this.form.getRawValue();
    this.service.saveToStorage(value);
    this.formChange.emit(value);
  }

  private handleTextInput(): void {
    this.subscription.add(
      this.textControl.valueChanges.subscribe((value) => {
        this.textControl.setValue(value.replace(/(\n+)/g, ' '), { emitEvent: false });
      })
    );
  }
}
