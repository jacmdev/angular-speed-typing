import { environment as commonEnvironment } from './environment.common';

export const environment = {
  ...commonEnvironment,
  apiUrl: 'http://localhost:3004',
  production: false,
};
