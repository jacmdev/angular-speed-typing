import { environment as commonEnvironment } from './environment.common';

export const environment = {
  ...commonEnvironment,
  apiUrl: '/api',
  production: true,
};
