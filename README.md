# About

Sample project measuring typing speed.

Project contains:

- List of exercises
- Exercise
    - Entered text validation
    - Time and result measure
    - Modal with result
- User defined exercise
    - Same as above, plus:
    - Form with validation
    - Saving exercise content in localStorage

# Demo

https://jacmdev.com/angular-speed-typing/

# Installation and use

1. Clone repository
2. Install required packages

```
npm ci
```

3. Run project

- fake API server with sample data

```
npm run api
```

- app live server

```
npm start
```

# Frameworks and libraries

- Angular - view, routing, forms
- Font Awesome - icons
- JSON Server - fake API
